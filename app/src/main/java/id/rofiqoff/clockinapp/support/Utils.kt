package id.rofiqoff.clockinapp.support

import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import id.rofiqoff.clockinapp.App
import id.rofiqoff.clockinapp.BuildConfig
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

const val TIME_OUT_ERROR_MESSAGE = "Sorry, there is something wrong :("

const val INTERNAL_SERVER_ERROR_MESSAGE = "Can not connect. Try again later :("

const val FORBIDDEN_ERROR_MESSAGE =
    "Your connection is not stable. Please check your internet connecction"

val baseUrl: String
    get() = BuildConfig.BASE_URL

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun showLog(m: String) {
    if (BuildConfig.DEBUG) {
        Log.e("debugLog ", m)
    }
}

fun showToast(message: String, isLong: Boolean = false) {
    val duration = if (isLong) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
    Toast.makeText(App.appContext, message, duration).show()
}

fun ViewGroup.showSnackbar(message: String, isLong: Boolean = false) {
    val duration = if (isLong) Snackbar.LENGTH_LONG else Snackbar.LENGTH_SHORT
    Snackbar.make(this, message, duration).show()
}

fun BottomSheetDialogFragment.makeFullScreen(
    rootView: View,
    params: CoordinatorLayout.LayoutParams
) {
    val parent = rootView.parent as View
    parent.fitsSystemWindows = true

    val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(parent)
    rootView.measure(0, 0)

    val displaymetrics = DisplayMetrics()

    activity?.windowManager?.defaultDisplay?.getMetrics(displaymetrics)

    val screenHeight = displaymetrics.heightPixels
    bottomSheetBehavior.peekHeight = screenHeight

    params.height = screenHeight
    parent.layoutParams = params
}

fun <T : ViewModel?> ViewModelStoreOwner.initViewModel(viewModelClass: Class<T>): T {
    return ViewModelProvider(this).get(viewModelClass)
}

fun setFormatRupiah(price: Double?): String {
    val localeId = Locale("in", "ID")
    val formatRupiah = NumberFormat.getCurrencyInstance(localeId)

    formatRupiah.maximumFractionDigits = 0
    formatRupiah.minimumFractionDigits = 0

    val rupiah = formatRupiah.format(price)

    return rupiah.replace("Rp", "Rp ")
}

fun setFormatWageType(type: String): String {
    return type.replace("_", " ")
}

fun <T : Any?> stringToObject(s: String, a: Class<T>): T {
    return Gson().fromJson(s, a)
}

fun objectToString(T: Any?): String {
    return Gson().toJson(T)
}

fun dateTimeNow(format: String): String {
    val date = Date();
    val formatter = SimpleDateFormat(format, Locale("ID"))
    return formatter.format(date)
}

fun dateTimeFormat(
    input: String?, format: String, formatResult: String, isFromUTC: Boolean = false
): String {
    val dateFormat = SimpleDateFormat(format, Locale.getDefault())
    if (isFromUTC) dateFormat.timeZone = TimeZone.getTimeZone("UTC")

    val dateFormatResult = SimpleDateFormat(formatResult, Locale("ID"))
    dateFormatResult.timeZone = TimeZone.getDefault()

    var date: Date? = Date()

    try {
        date = dateFormat.parse(input.toString())

    } catch (e: ParseException) {
        e.printStackTrace()
        showLog("error date time format : ${e.message}")
    }

    return dateFormatResult.format(date)
}
