package id.rofiqoff.clockinapp.support.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.rofiqoff.clockinapp.data.local.session.AppSession

abstract class BaseActivity(private val layoutId: Int) : AppCompatActivity() {

    val appSession by lazy { AppSession(baseContext.applicationContext) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        initView()
    }

    abstract fun initView()

}