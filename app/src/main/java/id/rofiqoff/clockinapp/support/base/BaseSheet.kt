package id.rofiqoff.clockinapp.support.base

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.rofiqoff.clockinapp.R
import id.rofiqoff.clockinapp.data.local.session.AppSession
import id.rofiqoff.clockinapp.support.makeFullScreen

abstract class BaseSheet(
    private var layoutId: Int,
    private var isFullScreen: Boolean = false
) : BottomSheetDialogFragment() {

    val appSession by lazy { AppSession(context?.applicationContext) }

    var behavior: CoordinatorLayout.Behavior<*>? = null
    lateinit var itemView: View

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), theme)

    lateinit var mDialog: Dialog

    var isDismis = false

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        isDismis = true
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        isDismis = true
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        itemView = LayoutInflater.from(context).inflate(layoutId, null, false)
        mDialog = dialog
        mDialog.setContentView(itemView)
        isDismis = false
        initView()
        layoutBehaviour()
    }

    private fun layoutBehaviour() {
        val params = (itemView.parent as View).layoutParams as CoordinatorLayout.LayoutParams
        behavior = params.behavior

        if (isFullScreen) makeFullScreen(itemView, params)

        if (behavior != null && behavior is BottomSheetBehavior<*>) {
            (behavior as BottomSheetBehavior<*>).addBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(p0: View, p1: Float) {}

                @SuppressLint("SwitchIntDef")
                override fun onStateChanged(p0: View, newState: Int) {
                    when (newState) {
                        BottomSheetBehavior.STATE_DRAGGING -> {
                        }
                        BottomSheetBehavior.STATE_SETTLING -> {
                        }
                        BottomSheetBehavior.STATE_EXPANDED -> {
                        }
                        BottomSheetBehavior.STATE_COLLAPSED -> {
                        }
                        BottomSheetBehavior.STATE_HIDDEN -> {
                            dismiss()
                        }
                    }
                }
            })
        }
    }

    fun hideLayout() {
        (behavior as BottomSheetBehavior<*>).state = BottomSheetBehavior.STATE_HIDDEN
    }

    abstract fun initView()
}