package id.rofiqoff.clockinapp.data.viewmodel.process

import id.rofiqoff.clockinapp.data.base.BaseRepository
import id.rofiqoff.clockinapp.data.model.request.ProcessRequest
import id.rofiqoff.clockinapp.data.network.apiservice.MainApi
import id.rofiqoff.clockinapp.data.network.support.ResponseCode
import id.rofiqoff.clockinapp.support.INTERNAL_SERVER_ERROR_MESSAGE
import id.rofiqoff.clockinapp.support.showLog

class ProcessRepository(private val api: MainApi) : BaseRepository() {

    suspend fun clockIn(request: ProcessRequest): Pair<Int, Any?>? {
        return try {
            val response = api.clockInAsync(authentication, request).await()

            requestApiCall { response }
        } catch (e: Exception) {
            showLog("message internal server clock in: ${e.message}")
            Pair(ResponseCode.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR_MESSAGE)
        }
    }

    suspend fun clockOut(request: ProcessRequest): Pair<Int, Any?>? {
        return try {
            val response = api.clockOutAsync(authentication, request).await()

            requestApiCall { response }
        } catch (e: Exception) {
            showLog("message internal server clock out: ${e.message}")
            Pair(ResponseCode.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR_MESSAGE)
        }
    }
}