package id.rofiqoff.clockinapp.data.local.session

import android.content.Context
import id.rofiqoff.clockinapp.data.model.response.ProfileResponse
import id.rofiqoff.clockinapp.support.objectToString
import id.rofiqoff.clockinapp.support.stringToObject

class AppSession(context: Context?) {
    var session = context?.getSharedPreferences("session", 0)
    var editor = session?.edit()

    private val tokenParam = "token-param"
    private val processParam = "process-param"
    private val dataStaffParam = "data-staff-param"
    private val clockInParam = "clock-in-param"
    private val clockOutParam = "clock-out-param"

    fun saveStaffInformation(data: ProfileResponse) {
        val dataJson = objectToString(data)
        editor?.apply { putString(dataStaffParam, dataJson) }?.apply()
    }

    fun saveClockTimeData(clockInTime: String?, clockOutTime: String?) {
        editor?.apply {
            putString(clockInParam, clockInTime)
            putString(clockOutParam, clockOutTime)
        }?.apply()
    }

    fun saveToken(token: String?) {
        editor?.putString(tokenParam, token)?.apply()
    }

    fun isClockInFinish(status: Boolean) {
        editor?.putBoolean(processParam, status)?.apply()
    }

    fun clearClockTimeData() {
        editor?.remove(clockInParam)
        editor?.remove(clockOutParam)
        editor?.apply()
    }

    val dataStaffInformation: ProfileResponse?
        get() {
            val dataSession = session?.getString(dataStaffParam, "").toString()
            return stringToObject(dataSession, ProfileResponse::class.java)
        }

    val token: String?
        get() = session?.getString(tokenParam, "")

    val isClockInFinish: Boolean?
        get() = session?.getBoolean(processParam, false)

    val clockInTime: String?
        get() = session?.getString(clockInParam, "-")

    val clockOutTime: String?
        get() = session?.getString(clockOutParam, "-")

}