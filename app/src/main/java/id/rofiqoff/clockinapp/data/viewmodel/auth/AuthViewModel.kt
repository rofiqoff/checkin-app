package id.rofiqoff.clockinapp.data.viewmodel.auth

import android.app.Application
import id.rofiqoff.clockinapp.data.base.BaseViewModel
import id.rofiqoff.clockinapp.data.model.request.AuthRequest
import id.rofiqoff.clockinapp.data.model.response.AuthResponse
import id.rofiqoff.clockinapp.data.model.response.ProfileResponse
import id.rofiqoff.clockinapp.data.network.support.AppApiClient
import id.rofiqoff.clockinapp.data.network.support.ResponseCode
import id.rofiqoff.clockinapp.support.FORBIDDEN_ERROR_MESSAGE
import id.rofiqoff.clockinapp.support.showLog
import kotlinx.coroutines.launch

class AuthViewModel(application: Application) : BaseViewModel(application) {

    private val repository by lazy { AuthRepository(AppApiClient.MainClient()) }

    fun login() {
        try {
            scope.launch {
                val dataLocal = appSession.dataStaffInformation

                if (dataLocal != null) {
                    data.postValue(Pair(false, Pair(ResponseCode.OK, dataLocal)))
                } else {
                    data.postValue(Pair(true, Pair(0, 0)))
                }

                val request = AuthRequest("+6281313272005", "alexander")

                val result = repository.login(request)

                when (val response = result?.second) {
                    is AuthResponse -> {
                        val key = response.key

                        appSession.saveToken(key)

                        getStaffInformation()
                    }
                }

            }
        } catch (e: Exception) {
            val messageError = e.message
            showLog("messageError login : $messageError")
            data.postValue(
                Pair(
                    false,
                    Pair(ResponseCode.FORBIDDEN, FORBIDDEN_ERROR_MESSAGE)
                )
            )
        }
    }

    private fun getStaffInformation() {
        try {
            scope.launch {
                val result = repository.getStaffInformation()
                when (val response = result?.second) {
                    is ProfileResponse -> appSession.saveStaffInformation(response)
                }

                data.postValue(Pair(false, result))
            }
        } catch (e: Exception) {
            val messageError = e.message
            showLog("messageError get staff information : $messageError")
            data.postValue(
                Pair(
                    false,
                    Pair(ResponseCode.FORBIDDEN, FORBIDDEN_ERROR_MESSAGE)
                )
            )
        }
    }

}