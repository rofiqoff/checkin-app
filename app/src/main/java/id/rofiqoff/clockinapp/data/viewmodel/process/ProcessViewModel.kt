package id.rofiqoff.clockinapp.data.viewmodel.process

import android.app.Application
import id.rofiqoff.clockinapp.data.base.BaseViewModel
import id.rofiqoff.clockinapp.data.model.request.ProcessRequest
import id.rofiqoff.clockinapp.data.model.response.ClockOutResponse
import id.rofiqoff.clockinapp.data.model.response.ProcessAlreadyResponse
import id.rofiqoff.clockinapp.data.model.response.ProcessResponse
import id.rofiqoff.clockinapp.data.network.support.AppApiClient
import id.rofiqoff.clockinapp.data.network.support.ResponseCode
import id.rofiqoff.clockinapp.support.*
import kotlinx.coroutines.launch

class ProcessViewModel(application: Application) : BaseViewModel(application) {

    private val repository by lazy { ProcessRepository(AppApiClient.MainClient()) }

    val formatPattern = "hh:mm a"
    val formatPatternUTC = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

    fun clockIn(latitude: String, longitude: String) {
        try {
            scope.launch {
                data.postValue(Pair(true, Pair(0, 0)))

                val request = ProcessRequest(latitude, longitude)

                val result = repository.clockIn(request)

                when (val resultResponse = result?.second) {
                    is ProcessResponse -> {
                        appSession.isClockInFinish(true)
                        val clockInTime = dateTimeFormat(
                            resultResponse.clock_in_time,
                            formatPatternUTC,
                            formatPattern,
                            true
                        )

                        appSession.saveClockTimeData(clockInTime, "-")
                    }
                    is String -> {
                        when (result.first) {
                            ResponseCode.BAD_REQUEST -> {
                                val data = stringToObject(
                                    resultResponse,
                                    ProcessAlreadyResponse::class.java
                                )
                                val detail = data.detail
                                if (detail?.contains("clocked in", true) == true) {
                                    appSession.isClockInFinish(true)
                                    appSession.saveClockTimeData(dateTimeNow(formatPattern), "-")
                                } else {
                                    appSession.saveClockTimeData(
                                        appSession.clockInTime,
                                        dateTimeNow(formatPattern)
                                    )
                                    appSession.isClockInFinish(false)
                                }
                            }
                        }
                    }
                }

                data.postValue(Pair(false, result))
            }
        } catch (e: Exception) {
            val messageError = e.message
            showLog("messageError clock in : $messageError")
            data.postValue(
                Pair(
                    false,
                    Pair(ResponseCode.FORBIDDEN, FORBIDDEN_ERROR_MESSAGE)
                )
            )
        }
    }

    fun clockOut(latitude: String, longitude: String) {
        try {
            scope.launch {
                data.postValue(Pair(true, Pair(0, 0)))

                val request = ProcessRequest(latitude, longitude)

                val result = repository.clockOut(request)

                when (val resultResponse = result?.second) {
                    is ClockOutResponse -> {
                        val data = resultResponse.timesheet
                        appSession.isClockInFinish(false)

                        val clockInTime =
                            dateTimeFormat(
                                data?.clock_in_time,
                                formatPatternUTC,
                                formatPattern,
                                true
                            )
                        val clockOutTime =
                            dateTimeFormat(
                                data?.clock_out_time,
                                formatPatternUTC,
                                formatPattern,
                                true
                            )

                        appSession.saveClockTimeData(clockInTime, clockOutTime)
                    }
                    is String -> {
                        when (result.first) {
                            ResponseCode.BAD_REQUEST -> {
                                val data = stringToObject(
                                    resultResponse,
                                    ProcessAlreadyResponse::class.java
                                )
                                val detail = data.detail
                                if (detail?.contains("clocked in", true) == true) {
                                    appSession.isClockInFinish(true)
                                    appSession.saveClockTimeData(dateTimeNow(formatPattern), "-")
                                } else {
                                    appSession.saveClockTimeData(
                                        appSession.clockInTime,
                                        dateTimeNow(formatPattern)
                                    )
                                    appSession.isClockInFinish(false)
                                }
                            }
                        }
                    }
                }

                data.postValue(Pair(false, result))
            }
        } catch (e: Exception) {
            val messageError = e.message
            showLog("messageError clock in : $messageError")
            data.postValue(
                Pair(
                    false,
                    Pair(ResponseCode.FORBIDDEN, FORBIDDEN_ERROR_MESSAGE)
                )
            )
        }
    }

}