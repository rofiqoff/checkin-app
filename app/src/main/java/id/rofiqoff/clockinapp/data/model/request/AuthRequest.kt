package id.rofiqoff.clockinapp.data.model.request

data class AuthRequest(var username: String, var password: String)