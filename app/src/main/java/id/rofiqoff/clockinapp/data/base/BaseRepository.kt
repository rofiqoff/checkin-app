package id.rofiqoff.clockinapp.data.base

import id.rofiqoff.clockinapp.App
import id.rofiqoff.clockinapp.data.local.session.AppSession
import id.rofiqoff.clockinapp.data.network.support.ResponseCode
import id.rofiqoff.clockinapp.data.network.support.Result
import id.rofiqoff.clockinapp.support.TIME_OUT_ERROR_MESSAGE
import id.rofiqoff.clockinapp.support.showLog
import retrofit2.Response

open class BaseRepository {

    val appSession by lazy { AppSession(App.appContext) }

    val authentication by lazy { "token ${appSession.token}" }

    suspend fun <T : Any> requestApiCall(call: suspend () -> Response<T>): Pair<Int, Any?> {

        return when (val result = resultApiCall(call)) {
            is Result.Success -> result.response
            is Result.Error -> {
                showLog("${result.response.second}")
                result.response
            }
        }
    }

    private suspend fun <T : Any> resultApiCall(call: suspend () -> Response<T>): Result<T> {
        return try {
            val response = call.invoke()

            val responseCode = response.code()

            return if (response.isSuccessful) {
                val responseBody = response.body()

                Result.Success(Pair(responseCode, responseBody))
            } else {
                Result.Error(Pair(responseCode, response.errorBody()?.string()))
            }
        } catch (error: Exception) {
            Result.Error(Pair(ResponseCode.TIME_OUT, TIME_OUT_ERROR_MESSAGE))
        }
    }
}