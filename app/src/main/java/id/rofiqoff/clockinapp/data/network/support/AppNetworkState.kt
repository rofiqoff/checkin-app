package id.rofiqoff.clockinapp.data.network.support

sealed class Result<out T : Any?> {
    data class Success<out T : Any?>(val response: Pair<Int, T?>) : Result<T>()
    data class Error(val response: Pair<Int, Any?>) : Result<Nothing>()
}