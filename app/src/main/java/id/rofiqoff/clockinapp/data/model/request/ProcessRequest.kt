package id.rofiqoff.clockinapp.data.model.request

data class ProcessRequest(var latitude: String, var longitude: String)