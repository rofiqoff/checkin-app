package id.rofiqoff.clockinapp.data.model.response

import com.google.gson.annotations.SerializedName

data class ProcessResponse(
    @SerializedName("id") var id: Int?,
    @SerializedName("clock_in_time") var clock_in_time: String?,
    @SerializedName("clock_out_time") var clock_out_time: String?,
    @SerializedName("clock_in_latitude") var clock_in_latitude: String?,
    @SerializedName("clock_in_longitude") var clock_in_longitude: String?,
    @SerializedName("clock_out_latitude") var clock_out_latitude: String?,
    @SerializedName("clock_out_longitude") var clock_out_longitude: String?
)

class ClockOutResponse(
    @SerializedName("timesheet") var timesheet: ProcessResponse?
)

data class ProcessAlreadyResponse(
    @SerializedName("code") var code: String?,
    @SerializedName("detail") var detail: String?
)