package id.rofiqoff.clockinapp.data.viewmodel.auth

import id.rofiqoff.clockinapp.data.base.BaseRepository
import id.rofiqoff.clockinapp.data.model.request.AuthRequest
import id.rofiqoff.clockinapp.data.network.apiservice.MainApi
import id.rofiqoff.clockinapp.data.network.support.ResponseCode
import id.rofiqoff.clockinapp.support.INTERNAL_SERVER_ERROR_MESSAGE
import id.rofiqoff.clockinapp.support.showLog

class AuthRepository(private val api: MainApi): BaseRepository(){

    suspend fun login(request: AuthRequest): Pair<Int, Any?>? {
        return try {
            val response = api.loginAsync(request).await()

            requestApiCall { response }
        } catch (e: Exception) {
            showLog("message internal server login: ${e.message}")
            Pair(ResponseCode.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR_MESSAGE)
        }
    }

    suspend fun getStaffInformation(): Pair<Int, Any?>? {
        return try {
            val response = api.getUserInformationAsync(authentication).await()

            requestApiCall { response }
        } catch (e: Exception) {
            showLog("message internal server get staff: ${e.message}")
            Pair(ResponseCode.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR_MESSAGE)
        }
    }

}