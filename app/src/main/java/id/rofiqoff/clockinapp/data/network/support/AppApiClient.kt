package id.rofiqoff.clockinapp.data.network.support

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import id.rofiqoff.clockinapp.BuildConfig
import id.rofiqoff.clockinapp.data.network.apiservice.MainApi
import id.rofiqoff.clockinapp.support.baseUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object AppApiClient {

    private val log = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }

    private val client = OkHttpClient.Builder().apply {
        if (BuildConfig.DEBUG) addInterceptor(log)
        retryOnConnectionFailure(true)
    }.build()

    private var retrofit = Retrofit.Builder()
        .client(client)
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    fun MainClient(): MainApi = retrofit.create(MainApi::class.java)
}