package id.rofiqoff.clockinapp.data.model.response

import com.google.gson.annotations.SerializedName

data class ProfileResponse(
    @SerializedName("id") var id: Int?,
    @SerializedName("wage_amount") var wage_amount: String?,
    @SerializedName("wage_type") var wage_type: String?,
    @SerializedName("client") var client: ClientResponse?,
    @SerializedName("location") var location: LocationResponse?,
    @SerializedName("position") var position: PositionResponse?,
    @SerializedName("manager") var manager: ManagerResponse?
)

data class ClientResponse(
    @SerializedName("id") var id: Int?,
    @SerializedName("name") var name: String?
)

data class LocationResponse(
    @SerializedName("address") var address: AddressResponse?
)

data class AddressResponse(
    @SerializedName("street_1") var street_1: String?,
    @SerializedName("street_2") var street_2: String?
)

data class PositionResponse(
    @SerializedName("id") var id: Int?,
    @SerializedName("name") var name: String?,
    @SerializedName("active") var active: Boolean?
)

data class ManagerResponse(
    @SerializedName("id") var id: String?,
    @SerializedName("name") var name: String?,
    @SerializedName("email") var email: String?,
    @SerializedName("phone") var phone: String?,
    @SerializedName("role") var role: String?
)