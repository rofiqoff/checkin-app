package id.rofiqoff.clockinapp.data.model.response

import com.google.gson.annotations.SerializedName

data class AuthResponse(
    @SerializedName("key") var key: String?
)