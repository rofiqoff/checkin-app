package id.rofiqoff.clockinapp.data.network.apiservice

import id.rofiqoff.clockinapp.data.model.request.AuthRequest
import id.rofiqoff.clockinapp.data.model.request.ProcessRequest
import id.rofiqoff.clockinapp.data.model.response.AuthResponse
import id.rofiqoff.clockinapp.data.model.response.ClockOutResponse
import id.rofiqoff.clockinapp.data.model.response.ProcessResponse
import id.rofiqoff.clockinapp.data.model.response.ProfileResponse
import id.rofiqoff.clockinapp.data.network.support.urlVersion
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface MainApi {

    @Headers("Content-Type: application/json")
    @POST("$urlVersion/auth/login/")
    fun loginAsync(@Body body: AuthRequest): Deferred<Response<AuthResponse>>

    @Headers("Content-Type: application/json")
    @POST("$urlVersion/staff-requests/26074/")
    fun getUserInformationAsync(
        @Header("Authorization") token: String
    ): Deferred<Response<ProfileResponse>>

    @Headers("Content-Type: application/json")
    @POST("$urlVersion/staff-requests/26074/clock-in/")
    fun clockInAsync(
        @Header("Authorization") token: String,
        @Body body: ProcessRequest
    ): Deferred<Response<ProcessResponse>>

    @Headers("Content-Type: application/json")
    @POST("$urlVersion/staff-requests/26074/clock-out/")
    fun clockOutAsync(
        @Header("Authorization") token: String,
        @Body body: ProcessRequest
    ): Deferred<Response<ClockOutResponse>>

}