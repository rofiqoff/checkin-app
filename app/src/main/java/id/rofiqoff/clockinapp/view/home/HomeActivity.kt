package id.rofiqoff.clockinapp.view.home

import androidx.lifecycle.Observer
import id.rofiqoff.clockinapp.R
import id.rofiqoff.clockinapp.data.model.response.ProfileResponse
import id.rofiqoff.clockinapp.data.viewmodel.auth.AuthViewModel
import id.rofiqoff.clockinapp.support.*
import id.rofiqoff.clockinapp.support.base.BaseActivity
import id.rofiqoff.clockinapp.view.process.ProcessSheet
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.loading_request
import kotlinx.android.synthetic.main.activity_home.text_client_name
import kotlinx.android.synthetic.main.activity_home.text_clock_in
import kotlinx.android.synthetic.main.activity_home.text_clock_out
import kotlinx.android.synthetic.main.activity_home.text_contact_number
import kotlinx.android.synthetic.main.activity_home.text_location
import kotlinx.android.synthetic.main.activity_home.text_location_manager
import kotlinx.android.synthetic.main.activity_home.text_position_name
import kotlinx.android.synthetic.main.activity_home.text_wage_amount
import kotlinx.android.synthetic.main.activity_home.text_wage_type

class HomeActivity : BaseActivity(R.layout.activity_home), ProcessSheet.OnActionListener {

    private val mViewModel by lazy { initViewModel(AuthViewModel::class.java) }

    private var isClockInFinish: Boolean? = false

    override fun initView() {
        mViewModel.login()
        observerViewModel()

        val textButton = when (appSession.isClockInFinish) {
            true -> getString(R.string.text_clock_out_title)
            else -> {
                appSession.clearClockTimeData()
                getString(R.string.text_clock_in_title)
            }
        }

        val clockInTimeFormat = appSession.clockInTime.toString()
        val clockOutTimeFormat = appSession.clockOutTime.toString()

        text_clock_out?.text = clockOutTimeFormat
        text_clock_in?.text = clockInTimeFormat

        button_clock?.text = textButton

        button_clock?.setOnClickListener {
            val processState = when (appSession.isClockInFinish) {
                true -> ProcessSheet.ProcessState.ClockOut
                else -> ProcessSheet.ProcessState.ClockIn
            }
            ProcessSheet.newInstance(this, processState).show(supportFragmentManager, "process")
        }
    }

    override fun onFinishRequest() {
        isClockInFinish = appSession.isClockInFinish

        val clockInTimeFormat = appSession.clockInTime.toString()
        val clockOutTimeFormat = appSession.clockOutTime.toString()

        val textButton = when {
            clockInTimeFormat != "-" && clockOutTimeFormat == "-" -> getString(R.string.text_clock_out_title)
            else -> ""
        }

        text_clock_in?.text = clockInTimeFormat
        text_clock_out?.text = clockOutTimeFormat

        button_clock?.apply {
            if (textButton == "") gone()
            text = textButton
        }
    }

    private fun observerViewModel() {
        mViewModel.data.observe(this, Observer {
            val code = it.second?.first
            val status = it.first

            loading_request?.apply { if (status) visible() else gone() }
            button_clock?.apply { if (status) gone() else visible() }

            when (val response = it.second?.second) {
                is ProfileResponse -> updateUI(response)
                is String -> showToast(response)
            }
        })
    }

    private fun updateUI(data: ProfileResponse?) {
        val positionName = data?.position?.name ?: "-"
        val clientName = data?.client?.name ?: "-"
        val wageAmount = data?.wage_amount ?: "0.0"
        val wageType = data?.wage_type ?: "-"
        val street1 = data?.location?.address?.street_1 ?: "-"
        val street2 = data?.location?.address?.street_2 ?: ""
        val locationName = "$street1\n$street2"
        val managerName = data?.manager?.name ?: "-"
        val managerPhone = data?.manager?.phone ?: "-"

        text_position_name?.text = positionName
        text_client_name?.text = clientName
        text_wage_amount?.text = setFormatRupiah(wageAmount.toDouble())
        text_wage_type?.text = setFormatWageType(wageType)
        text_location?.text = locationName
        text_location_manager?.text = managerName
        text_contact_number?.text = managerPhone
    }
}
