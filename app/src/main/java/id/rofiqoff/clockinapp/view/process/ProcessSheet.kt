package id.rofiqoff.clockinapp.view.process

import android.Manifest
import android.animation.ValueAnimator
import android.content.pm.PackageManager
import android.location.Location
import android.view.animation.LinearInterpolator
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import id.rofiqoff.clockinapp.R
import id.rofiqoff.clockinapp.data.model.response.ClockOutResponse
import id.rofiqoff.clockinapp.data.model.response.ProcessResponse
import id.rofiqoff.clockinapp.data.network.support.ResponseCode
import id.rofiqoff.clockinapp.data.viewmodel.process.ProcessViewModel
import id.rofiqoff.clockinapp.support.*
import id.rofiqoff.clockinapp.support.base.BaseSheet
import kotlinx.android.synthetic.main.item_process.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ProcessSheet : BaseSheet(R.layout.item_process, true) {

    enum class ProcessState {
        ClockIn, ClockOut
    }

    private val mViewModel by lazy { initViewModel(ProcessViewModel::class.java) }

    private var mAnimator: ValueAnimator? = null

    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null

    private val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 159
    private var mLocationPermissionGranted = false

    private var mLastKnownLocation: Location? = null

    private var mMyLatitude: Double? = 0.0
    private var mMyLongitude: Double? = 0.0

    companion object {

        lateinit var mListener: OnActionListener

        lateinit var mProcessState: ProcessState

        fun newInstance(listener: OnActionListener, processState: ProcessState): ProcessSheet {
            mListener = listener
            mProcessState = processState

            return ProcessSheet()
        }
    }

    override fun initView() {
        observerViewModel()

        mFusedLocationProviderClient = activity?.let {
            LocationServices.getFusedLocationProviderClient(it)
        }

        getLocationPermission()

        getDeviceLocation()

        if (mLocationPermissionGranted) initAnimationProgress()

        val statusProcess = when (mProcessState) {
            ProcessState.ClockIn -> getString(R.string.text_clock_in_status)
            ProcessState.ClockOut -> getString(R.string.text_clock_out_status)
        }

        itemView.text_process_status?.text = statusProcess

        itemView.button_cancel?.setOnClickListener {
            mAnimator?.cancel()
            mAnimator?.removeAllUpdateListeners()

            hideLayout()
        }
    }

    override fun onDestroy() {
        mAnimator?.cancel()
        mAnimator?.removeAllUpdateListeners()

        super.onDestroy()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        mLocationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true

                    initView()
                } else {
                    hideLayout()
                }
            }
        }
        getDeviceLocation()
    }

    private fun initAnimationProgress() {
        itemView.progress_bar?.viewTreeObserver?.addOnGlobalLayoutListener { startAnimation() }
    }

    private fun startAnimation() {
        val width = itemView.progress_bar.width
        itemView.progress_bar?.max = width

        mAnimator = ValueAnimator.ofInt(0, width)
        mAnimator?.interpolator = LinearInterpolator()
        mAnimator?.startDelay = 0
        mAnimator?.duration = 10_000

        mAnimator?.start()

        mAnimator?.addUpdateListener {
            val value = it.animatedValue as Int
            itemView.progress_bar?.progress = value

            if (value == width) request()
        }
    }

    private fun request() {
        itemView.loading_request?.visible()
        itemView.button_cancel?.invisible()

        if (mMyLatitude == null && mMyLongitude == null) {
            showToast("Please check your gps setting. And then back here again")
            GlobalScope.launch {
                delay(1000)
                super.hideLayout()
            }
        } else {
            processRequest()
        }
    }

    private fun getLocationPermission() {
        if (context?.applicationContext?.let {
                ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_FINE_LOCATION)
            } == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true
        } else {
            mLocationPermissionGranted = false
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    private fun getDeviceLocation() {
        try {
            if (mLocationPermissionGranted) {
                mFusedLocationProviderClient?.lastLocation.run {
                    this?.addOnCompleteListener {
                        if (it.isSuccessful) {
                            mLastKnownLocation = it.result
                            mMyLatitude = mLastKnownLocation?.latitude
                            mMyLongitude = mLastKnownLocation?.longitude
                        }
                    }
                }
            }
        } catch (e: SecurityException) {
            showLog("exception device location: ${e.message}")
        } catch (e: Exception) {
            showLog("exception get location: ${e.message}")
        }
    }

    private fun processRequest() {
        if (isDismis) {
            showLog("hidden")
        } else {
            when (mProcessState) {
                ProcessState.ClockIn -> {
                    mViewModel.clockIn(mMyLatitude.toString(), mMyLongitude.toString())
                }
                ProcessState.ClockOut -> {
                    mViewModel.clockOut(mMyLatitude.toString(), mMyLongitude.toString())
                }
            }
        }
    }

    private fun observerViewModel() {
        mViewModel.data.observe(this, Observer {
            val code = it.second?.first
            val status = it.first

            itemView.loading_request?.apply { if (status) visible() else invisible() }
            itemView.button_cancel?.apply { if (status) invisible() else visible() }

            when (val response = it.second?.second) {
                is ProcessResponse -> hideUI()
                is ClockOutResponse -> hideUI()
                is String -> {
                    when (code) {
                        ResponseCode.BAD_REQUEST -> {
                        }
                        else -> showToast(response)
                    }
                    hideUI()
                }
            }
        })
    }

    private fun hideUI() {
        mListener.onFinishRequest()
        super.hideLayout()
    }

    interface OnActionListener {
        fun onFinishRequest()
    }
}